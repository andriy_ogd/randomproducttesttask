﻿using System.Linq;
using RandomProduct.Core.Products;
using Xunit;

namespace RandomProduct.Tests.Basket
{
    public class BasketTests
    {
        [Fact]
        public void Add_AddsProductCorrectly()
        {
            RandomProduct.Basket.Basket.Basket.ClearInstance();
            var basket = RandomProduct.Basket.Basket.Basket.GetInstance();

            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));
            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));

            var info = basket.GetBasketInfo();

            Assert.Equal(17.9M, info.SubTotal);
            Assert.Single(info.Positions);
            Assert.Equal(2, info.Positions.Single().Quantity);
        }

        [Fact]
        public void Remove_RemovesProductCorrectly()
        {
            RandomProduct.Basket.Basket.Basket.ClearInstance();
            var basket = RandomProduct.Basket.Basket.Basket.GetInstance();

            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));
            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));
            basket.Remove("RP-5NS-DITB");

            var info = basket.GetBasketInfo();

            Assert.Equal(8.95M, info.SubTotal);
            Assert.Single(info.Positions);
            Assert.Equal(1, info.Positions.Single().Quantity);
        }

        [Fact]
        public void RemoveRange_RemovesCorrectly()
        {
            RandomProduct.Basket.Basket.Basket.ClearInstance();
            var basket = RandomProduct.Basket.Basket.Basket.GetInstance();

            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));
            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));
            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));
            basket.Remove("RP-5NS-DITB", 2);

            var info = basket.GetBasketInfo();

            Assert.Equal(8.95M, info.SubTotal);
            Assert.Single(info.Positions);
            Assert.Equal(1, info.Positions.Single().Quantity);
        }

        [Fact]
        public void Empty_RemovesAllItems()
        {
            RandomProduct.Basket.Basket.Basket.ClearInstance();
            var basket = RandomProduct.Basket.Basket.Basket.GetInstance();

            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));
            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));
            basket.Add(new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M));
            basket.Empty();

            var info = basket.GetBasketInfo();

            Assert.Equal(0M, info.SubTotal);
            Assert.Empty(info.Positions);
        }
    }
}
