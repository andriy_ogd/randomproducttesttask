﻿using RandomProduct.Basket.Basket;
using System;
using System.Collections.Generic;
using Xunit;

namespace RandomProduct.Tests
{
    public class BasketInfoTests
    {
        [Fact]
        public void BasketInfo_ShouldCalculateSubTotalCorrectly()
        {
            BasketInfo info = new BasketInfo();

            info.Positions = new List<BasketPosition>
            {
                new BasketPosition {ProductName = "TestProduct1", Quantity = 2, SubTotal = 13M, Total = 11M},
                new BasketPosition {ProductName = "TestProduct2", Quantity = 3, SubTotal = 2.5M, Total = 2.0M}
            };

            Assert.Equal(15.5M, info.SubTotal);
        }

        [Fact]
        public void BasketInfo_ShouldCalculateTotalCorrectly()
        {
            BasketInfo info = new BasketInfo();

            info.Positions = new List<BasketPosition>
            {
                new BasketPosition {ProductName = "TestProduct1", Quantity = 2, SubTotal = 13M, Total = 11M},
                new BasketPosition {ProductName = "TestProduct2", Quantity = 3, SubTotal = 2.5M, Total = 2.0M}
            };

            Assert.Equal(13M, info.FinalPrice);
        }
    }
}
