﻿using RandomProduct.Core.Discounts;
using RandomProduct.Core.Products;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace RandomProduct.Tests.Discounts
{
    public class DiscountTests
    {
        #region MockData
        static List<Product> _products = new List<Product>
        {
            new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M),
            new Product("RP-25D-SITB", "Bag of Pogs", "25 Random pogs designs.", 5.31M),
            new Product("RP-1TB-EITB", "Large bowl of Trifle", "Large collectors edition bowl of Trifle.", 2.75M),
            new Product("RP-RPM-FITB", "Paper Mask", "Randomly selected paper mask.", 0.30M)
        };

        static List<Discount> _discounts = new List<Discount>
        {
            new Discount(
                1,
                "Buy 2 or more Bags of Pogs and get 50% off each bag (excluding the first one).",
                new QuantityOfProductDiscountRule("RP-25D-SITB", 2),
                new CustomProductDiscountResult("RP-25D-SITB", 2, 0.5M)),
            new Discount(
                2,
                "Buy a Large bowl of Trifle and get a free Paper Mask.",
                new QuantityOfProductDiscountRule("RP-1TB-EITB", 1),
                new FreeProductDiscountResult(_products[3])),
            new Discount(
                3,
                "Buy 100 or more Shurikens and get 30% off whole basket.",
                new QuantityOfProductDiscountRule("RP-5NS-DITB", 5),
                new WholeBasketDiscountResult(0.7M))
        }; 
        #endregion

        [Fact]
        public void FreeDiscountResult_ShouldAddFreeProduct()
        {
            RandomProduct.Basket.Basket.Basket.ClearInstance();
            var basket = RandomProduct.Basket.Basket.Basket.GetInstance();
            basket.AvailableDiscounts = _discounts;

            basket.Add(_products[2]);

            var info = basket.GetBasketInfo();

            Assert.Equal(2, info.Positions.Count());
            Assert.Equal(2.75M, info.FinalPrice);
        }

        [Fact]
        public void CustomProductDiscountResult_ShouldAddDiscountForEachProduct()
        {
            RandomProduct.Basket.Basket.Basket.ClearInstance();
            var basket = RandomProduct.Basket.Basket.Basket.GetInstance();
            basket.AvailableDiscounts = _discounts;

            basket.Add(_products[1]); //5.31 - 100%
            basket.Add(_products[1]); //2.655 - 50%
            basket.Add(_products[1]); //2.655 - 50%

            var info = basket.GetBasketInfo();

            Assert.Single(info.Positions);
            Assert.Equal(3, info.Positions.Single().Quantity);
            Assert.Equal(10.62M, info.FinalPrice);
        }

        [Fact]
        public void WholeCartDiscountResult_ShouldAddDiscountForWholeCart()
        {
            RandomProduct.Basket.Basket.Basket.ClearInstance();

            var basket = RandomProduct.Basket.Basket.Basket.GetInstance();
            basket.AvailableDiscounts = _discounts;

            for (int i = 0; i < 5; i++)
            {
                basket.Add(_products[0]); //full price - 44.75M
            }

            var info = basket.GetBasketInfo();

            //full price - 44.75M
            //70% - 31.325M


            Assert.Equal(44.75M, info.SubTotal);
            Assert.Equal(31.325M, info.FinalPrice);
        }

        // TODO: Here might be more tests
    }
}
