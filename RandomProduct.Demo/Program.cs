﻿using RandomProduct.Basket.Basket;
using RandomProduct.Core.Discounts;
using RandomProduct.Core.Products;
using System;
using System.Collections.Generic;

namespace RandomProduct.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            var basket = Basket.Basket.Basket.GetInstance();
            basket.AvailableDiscounts = _discounts;

            basket.Add(_products[1]);
            basket.Add(_products[2]);
            basket.Add(_products[1]);

            //breakpoint here to watch info
            var info = basket.GetBasketInfo();
        }

        static List<Product> _products = new List<Product>
        {
            new Product("RP-5NS-DITB", "Shurikens", "5 pointed Shurikens made from stainless steel.", 8.95M),
            new Product("RP-25D-SITB", "Bag of Pogs", "25 Random pogs designs.", 5.31M),
            new Product("RP-1TB-EITB", "Large bowl of Trifle", "Large collectors edition bowl of Trifle.", 2.75M),
            new Product("RP-RPM-FITB", "Paper Mask", "Randomly selected paper mask.", 0.30M)
        };

        static List<Discount> _discounts = new List<Discount>
        {
            new Discount(
                1,
                "Buy 2 or more Bags of Pogs and get 50% off each bag (excluding the first one).",
                new QuantityOfProductDiscountRule("RP-25D-SITB", 2),
                new CustomProductDiscountResult("RP-25D-SITB", 2, 0.5M)),
            new Discount(
                2,
                "Buy a Large bowl of Trifle and get a free Paper Mask.",
                new QuantityOfProductDiscountRule("RP-1TB-EITB", 1),
                new FreeProductDiscountResult(_products[3])),
            new Discount(
                3,
                "Buy 100 or more Shurikens and get 30% off whole basket.",
                new QuantityOfProductDiscountRule("RP-5NS-DITB", 100),
                new WholeBasketDiscountResult(0.7M))
        };
    }
}
