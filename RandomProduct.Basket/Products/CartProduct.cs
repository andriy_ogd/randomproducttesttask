﻿using RandomProduct.Core.Products;

namespace RandomProduct.Basket.Products
{
    public class CartProduct : Product
    {
        public CartProduct(Product product, decimal finalPrice) : base(product.Id, product.ProductName, product.Description, product.Price)
        {
            FinalPrice = finalPrice;
        }

        public decimal FinalPrice { get; set; }
    }
}
