﻿using RandomProduct.Core.Discounts;
using RandomProduct.Core.Products;
using System.Collections.Generic;
using System.Linq;

namespace RandomProduct.Basket.Basket
{
    /// <summary>
    /// Singleton which holds info about products in cart and discounts applied
    /// </summary>
    public class Basket
    {
        #region Singleton

        private static Basket _instance;

        private Basket()
        {
            _addedProducts = new List<CartProduct>();
            _appliedDiscounts = new List<Discount>();
            _basketProductsWithDiscounts = new List<CartProduct>();
        }

        /// <summary>
        /// Used to create or retrieve instance of Basket 
        /// </summary>
        /// <returns>Singleton instance</returns>
        public static Basket GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Basket();
            }

            return _instance;
        }

        public static void ClearInstance()
        {
            _instance = null;
        }

        #endregion

        /// <summary>
        /// Products added by customer
        /// </summary>
        private List<CartProduct> _addedProducts;

        /// <summary>
        /// Products list after discounts apply
        /// </summary>
        private List<CartProduct> _basketProductsWithDiscounts;

        /// <summary>
        /// List of applied discounts
        /// </summary>
        private List<Discount> _appliedDiscounts;

        /// <summary>
        /// All currently available discounts
        /// </summary>
        public List<Discount> AvailableDiscounts { get; set; }

        /// <summary>
        /// Adds product to basket
        /// </summary>
        /// <param name="product">Product to add</param>
        public void Add(Product product)
        {
            //Casting product to cart product as FinalPrice needed to hold price with discounts
            CartProduct cartProduct = new CartProduct(product, product.Price);

            _addedProducts.Add(cartProduct);
        }

        /// <summary>
        /// Removes product(s) with given id from cart
        /// </summary>
        /// <param name="id">id of product to remove</param>
        /// <param name="quantity">quantity of product to remove</param>
        public void Remove(string id, int quantity = 1)
        {
            var toDelete = new List<CartProduct>(_addedProducts.Where(p => p.Id == id).Take(quantity));_addedProducts.Where(p => p.Id == id).Take(quantity);

            foreach (var product in toDelete)
            {
                _addedProducts.Remove(product);
            }
        }

        /// <summary>
        /// Clears basket
        /// </summary>
        public void Empty()
        {
            _addedProducts.Clear();
        }

        /// <summary>
        /// Returns info about final product list in basket, applied discounts, total amount
        /// </summary>
        /// <returns>BasketInfo object</returns>
        public BasketInfo GetBasketInfo()
        {
            ApplyDiscounts();

            return new BasketInfo
            {
                AppliedDiscounts = _appliedDiscounts,
                Positions = _basketProductsWithDiscounts.GroupBy(p => p.Id).Select(p => new BasketPosition
                {
                    ProductName = p.First().ProductName,
                    Quantity = p.Count(),
                    SubTotal = p.Sum(cp => cp.Price),
                    Total = p.Sum(cp => cp.FinalPrice)
                })
            };
        }

        /// <summary>
        /// Iterates each available discount, if applicable applies
        /// </summary>
        private void ApplyDiscounts()
        {
            _basketProductsWithDiscounts = new List<CartProduct>(_addedProducts);

            _basketProductsWithDiscounts.ForEach(p => p.FinalPrice = p.Price);

            if (AvailableDiscounts == null) return;

            foreach (var discount in AvailableDiscounts)
            {
                if (discount.DiscountRule.Applicable(_addedProducts))
                {
                    if (_appliedDiscounts.All(d => d.Id != discount.Id))
                    {
                        _appliedDiscounts.Add(discount);
                    }

                    _basketProductsWithDiscounts = discount.DiscountResult.Apply(_basketProductsWithDiscounts);
                }
            }
        }
    }
}
