﻿using RandomProduct.Core.Discounts;
using System.Collections.Generic;
using System.Linq;

namespace RandomProduct.Basket.Basket
{
    /// <summary>
    /// Represents products added to cart, applied discounts, subtotal and total
    /// </summary>
    public class BasketInfo
    {
        /// <summary>
        /// one for each product with quantity subtotal and total
        /// </summary>
        public IEnumerable<BasketPosition> Positions { get; set; }

        /// <summary>
        /// All discounts was applied to current basket
        /// </summary>
        public IEnumerable<Discount> AppliedDiscounts { get; set; }

        /// <summary>
        /// Full price of products added to basket
        /// </summary>
        public decimal SubTotal => Positions.Sum(p => p.SubTotal);

        /// <summary>
        /// Price with discounts
        /// </summary>
        public decimal FinalPrice => Positions.Sum(p => p.Total);
    }
}
