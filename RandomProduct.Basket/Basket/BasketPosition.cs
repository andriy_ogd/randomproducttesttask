﻿namespace RandomProduct.Basket.Basket
{
    /// <summary>
    /// Represents product in cart, quantity of ordered products, subtotal and total
    /// </summary>
    public class BasketPosition
    {
        public string ProductName { get; set; }

        public int Quantity { get; set; }

        public decimal SubTotal { get; set; }

        public decimal Total { get; set; }
    }
}
