﻿namespace RandomProduct.Core.Products
{
    /// <summary>
    /// This model represents Product entity, like it can be stored in DB
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Constructor of product
        /// </summary>
        /// <param name="id">Product id</param>
        /// <param name="name">Product name</param>
        /// <param name="description">Product description</param>
        /// <param name="price">Product price</param>
        public Product(string id, string name, string description, decimal price)
        {
            Id = id;
            ProductName = name;
            Description = description;
            Price = price;
        }

        public string Id { get; set; }

        public string ProductName { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }
    }
}
