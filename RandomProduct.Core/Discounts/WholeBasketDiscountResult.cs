﻿using RandomProduct.Core.Products;
using System.Collections.Generic;
using System.Linq;

namespace RandomProduct.Core.Discounts
{
    /// <summary>
    /// Used when discount applied for whole basket total
    /// </summary>
    public class WholeBasketDiscountResult : DiscountResultBase
    {
        public WholeBasketDiscountResult(decimal multiplier)
        {
            Multiplier = multiplier;
        }

        /// <summary>
        /// price multiplier. if 30% off - multiplier should be 0.7M
        /// </summary>
        public decimal Multiplier { get; set; }

        public override List<CartProduct> Apply(List<CartProduct> products)
        {
            var productsWithDiscount = new List<CartProduct>(products);
            
            productsWithDiscount = productsWithDiscount.Select(p =>
            {
                p.FinalPrice *= Multiplier;

                return p;
            }).ToList();

            return productsWithDiscount;
        }
    }
}
