﻿using System;
using System.Collections.Generic;
using System.Text;
using RandomProduct.Core.Products;

namespace RandomProduct.Core.Discounts
{
    /// <summary>
    /// Base class declares method which accepts list of products in cart and returns list with discounts
    /// </summary>
    public abstract class DiscountResultBase
    {
        public abstract List<CartProduct> Apply(List<CartProduct> products);
    }
}
