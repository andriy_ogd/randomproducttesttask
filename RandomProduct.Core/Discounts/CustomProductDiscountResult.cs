﻿using RandomProduct.Core.Products;
using System.Collections.Generic;
using System.Linq;

namespace RandomProduct.Core.Discounts
{
    /// <summary>
    /// Used if you want to make discount for concrete product (with ProductId), starting from defined quantity
    /// Ex. if you want to make discount for each product with id = RP-25D-SITB c starting from 2nd position for 40% off
    /// use next values ProductId = "RP-25D-SITB", Multiplier = 0.6M, StartFromPosition = 2
    /// </summary>
    public class CustomProductDiscountResult : DiscountResultBase
    {
        public CustomProductDiscountResult(string productId, int startFromPosition, decimal multiplier)
        {
            ProductId = productId;
            StartFromPosition = startFromPosition;
            Multiplier = multiplier;
        }

        /// <summary>
        /// From what quantity start to apply discount
        /// </summary>
        public int StartFromPosition { get; set; }

        /// <summary>
        /// Price multiplier - if 30% off multiplier should be 0.7M
        /// </summary>
        public decimal Multiplier { get; set; }

        /// <summary>
        /// Id of product for which discount will be applied
        /// </summary>
        public string ProductId { get; set; }

        public override List<CartProduct> Apply(List<CartProduct> products)
        {
            var finalProductList = new List<CartProduct>(products);

            var productsWithDiscount = finalProductList.Where(p => p.Id == ProductId).Skip(StartFromPosition - 1);
            foreach (var product in productsWithDiscount)
            {
                product.FinalPrice *= Multiplier;
            }

            return finalProductList;
        }
    }
}
