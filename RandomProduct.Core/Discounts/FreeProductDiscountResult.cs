﻿using RandomProduct.Core.Products;
using System.Collections.Generic;

namespace RandomProduct.Core.Discounts
{
    /// <summary>
    /// Discount result used if you want to add product to order for free
    /// </summary>
    public class FreeProductDiscountResult : DiscountResultBase
    {
        public FreeProductDiscountResult(Product product)
        {
            Product = product;
        }

        /// <summary>
        /// Product which will be added for free
        /// </summary>
        public Product Product { get; set; }

        public override List<CartProduct> Apply(List<CartProduct> products)
        {
            var cartProduct = new CartProduct(Product, 0M);

            var productsWithDiscount = new List<CartProduct>(products)
            {
                cartProduct
            };

            return productsWithDiscount;
        }
    }
}
