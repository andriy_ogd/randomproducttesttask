﻿namespace RandomProduct.Core.Discounts
{
    /// <summary>
    /// Definition of discount
    /// </summary>
    public class Discount
    {
        /// <summary>
        /// Discount ctor
        /// </summary>
        /// <param name="id">unique discount id</param>
        /// <param name="description">discount description</param>
        /// <param name="discountRule">object which describes the case when discount will be applied</param>
        /// <param name="discountResult">object which applies discount</param>
        public Discount(int id, string description, DiscountRuleBase discountRule, DiscountResultBase discountResult)
        {
            Id = id;
            Description = description;
            DiscountRule = discountRule;
            DiscountResult = discountResult;
        }

        public int Id { get; set; }

        public string Description { get; set; }

        public DiscountRuleBase DiscountRule { get; set; }

        public DiscountResultBase DiscountResult { get; set; }
    }
}
