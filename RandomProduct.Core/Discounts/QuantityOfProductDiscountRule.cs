﻿using System.Collections.Generic;
using System.Linq;
using RandomProduct.Core.Products;

namespace RandomProduct.Core.Discounts
{
    /// <summary>
    /// Rule which can be used in case if discount condition is to buy defined quantity of concrete product or more
    /// </summary>
    public class QuantityOfProductDiscountRule : DiscountRuleBase
    {
        public QuantityOfProductDiscountRule(string productId, int minimalQuantity)
        {
            ProductId = productId;
            MinimalQuantity = minimalQuantity;
        }

        /// <summary>
        /// Id of product that need to be purchased for this rule
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// Minimal quantity of product for this rule
        /// </summary>
        public int MinimalQuantity { get; set; }


        public override bool Applicable(IEnumerable<Product> products)
        {
            return products.Count(p => p.Id == ProductId) >= MinimalQuantity;
        }
    }
}
