﻿using System;
using System.Collections.Generic;
using System.Text;
using RandomProduct.Core.Products;

namespace RandomProduct.Core.Discounts
{
    /// <summary>
    /// Base class which declares method which decides if discount can be applied
    /// </summary>
    public abstract class DiscountRuleBase
    {
        public abstract bool Applicable(IEnumerable<Product> products);
    }
}
